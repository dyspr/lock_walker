var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0],
  walker: 255
}

var boardSize
var walker
var nextPos = null
var nextSize
var walkerHistory = []
var size = 3
var array = create2DArray(size, size, 0, true)
var resolution = 64

var update = 0
var frame = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
  walker = [Math.floor(size * 0.5), Math.floor(size * 0.5), 1]
  array[walker[0]][walker[1]] = 1

  array = create2DArray(size, size, 0, true)
  array[walker[0]][walker[1]] = 1
  nextPos = null

  while (nextPos !== undefined) {
    walkerHistory.push([...walker])
    nextPos = getNextPos(array)
    nextSize = 0.1 + Math.random() * 0.9
    if (nextPos !== undefined) {
      for (var i = 0; i < resolution; i++) {
        walkerHistory.push([walker[0] + (nextPos[0] - walker[0]) * (1 / resolution) * i, walker[1] + (nextPos[1] - walker[1]) * (1 / resolution) * i, walker[2] + (nextSize - walker[2]) * (1 / resolution) * i])
      }
      walker[0] = nextPos[0]
      walker[1] = nextPos[1]
      walker[2] = nextSize
      array[walker[0]][walker[1]] = 1
    }
  }

  array = create2DArray(size, size, 0, true)
  array[walker[0]][walker[1]] = 1
  nextPos = null

  while (nextPos !== undefined) {
    walkerHistory.push([...walker])
    nextPos = getNextPos(array)
    nextSize = 0.1 + Math.random() * 0.9
    if (nextPos !== undefined) {
      for (var i = 0; i < resolution; i++) {
        walkerHistory.push([walker[0] + (nextPos[0] - walker[0]) * (1 / resolution) * i, walker[1] + (nextPos[1] - walker[1]) * (1 / resolution) * i, walker[2] + (nextSize - walker[2]) * (1 / resolution) * i])
      }
      walker[0] = nextPos[0]
      walker[1] = nextPos[1]
      walker[2] = nextSize
      array[walker[0]][walker[1]] = 1
    }
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = resolution * 8 + 9; i < resolution * 16 + 18; i++) {
    if (walkerHistory.length !== 0) {
      fill(colors.walker * ((i - (resolution * 8 + 9)) / (resolution * 8 + 9)))
      noStroke()
      ellipse(windowWidth * 0.5 + (walkerHistory[i][0] - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), windowHeight * 0.5 + (walkerHistory[i][1] - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size), (44 / 768) * boardSize * (17 / size) * walkerHistory[i][2] * 0.5, (44 / 768) * boardSize * (17 / size) * walkerHistory[i][2] * 0.5)
    }
  }

  update += deltaTime
  if (update > 1) {
    update = 0

    walkerHistory.splice(0, 1)

    if (walkerHistory.length < resolution * 16 + 18) {
      array = create2DArray(size, size, 0, true)
      array[walker[0]][walker[1]] = 1
      nextPos = null

      while (nextPos !== undefined) {
        walkerHistory.push([...walker])
        nextPos = getNextPos(array)
        nextSize = 0.1 + Math.random() * 0.9
        if (nextPos !== undefined) {
          for (var i = 0; i < resolution; i++) {
            walkerHistory.push([walker[0] + (nextPos[0] - walker[0]) * (1 / resolution) * i, walker[1] + (nextPos[1] - walker[1]) * (1 / resolution) * i, walker[2] + (nextSize - walker[2]) * (1 / resolution) * i])
          }
          walker[0] = nextPos[0]
          walker[1] = nextPos[1]
          walker[2] = nextSize
          array[walker[0]][walker[1]] = 1
        }
      }
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function getNextPos(array) {
  var possibleNextPos = []
  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      if (array[i][j] !== 1) {
        possibleNextPos.push([i, j])
      }
    }
  }
  var randomPick = possibleNextPos[Math.floor(Math.random() * possibleNextPos.length)]
  return randomPick
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
